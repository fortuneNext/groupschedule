import {Injectable} from '@angular/core';
import {AngularFireDatabase, AngularFireList} from '@angular/fire/database';
import {DateRow} from './app/date-row';
import {map} from 'rxjs/operators';
import {AngularFireFunctions} from '@angular/fire/functions';

@Injectable({
  providedIn: 'root'
})
export class FireBaseProviderService {
  public dbList: AngularFireList<{ date: string, red: string[], yellow: string[] }>;

  constructor(public db: AngularFireDatabase, public functions: AngularFireFunctions) {
    this.dbList = this.db.list('/datesDev');
    this.dbList.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
      )
    ).subscribe(dates => {
      this.cleanDb(dates);
    });
  }

  public getDatesObservable() {
    return this.dbList.valueChanges();
  }

  public updateDate(dateRow: DateRow) {
    const subscription = this.dbList.snapshotChanges().pipe(
      map(changes =>
        changes.map(c => ({key: c.payload.key, ...c.payload.val()}))
      )
    ).subscribe(dates => {
      const existingDate = dates.find(elem => elem.date === dateRow.date.toDateString());
      const update = {
        date: dateRow.date.toDateString(),
        yellow: dateRow.yellow,
        red: dateRow.red
      };
      if (existingDate) {
        this.dbList.update(existingDate.key, update);
        this.logIt(`Updated ${existingDate.key} with ${JSON.stringify(update)}`);
      } else {
        this.dbList.push(update);
        this.logIt(`Created new row with ${JSON.stringify(update)}`);
      }
      subscription.unsubscribe();
    });
  }

  public logIt(text: string, warn?: boolean) {
    if (warn) {
      this.functions.httpsCallable(`warnIt?text=${text}`)({}).subscribe();
    } else {
      this.functions.httpsCallable(`logIt?text=${text}`)({}).subscribe();
    }
  }

  private cleanDb(dates: any[]) {
    for (const date of dates) {
      // if ((!date.hasOwnProperty('red') || date['red'].length <= 0)
      //   && (!date.hasOwnProperty('yellow') || date['yellow'].length <= 0)) {
      //   console.log(date);
      // }
      for (const date2 of dates) {
        if (date.key !== date2.key && date.date === date2.date) {
          this.logIt(`Found duplicate ${date.date}! ${date.key} vs ${date2.key}. Removing ${date.key}...`, true);
          this.dbList.remove(date.key);
        }
      }
      const dateItem = new Date(Date.parse(date.date));
      dateItem.setDate(dateItem.getDate() + 1);
      if (dateItem < new Date(Date.now()) && date.key) {
        this.logIt(`Removing old row ${JSON.stringify(date)}`);
        this.dbList.remove(date.key);
      }
    }
  }
}
