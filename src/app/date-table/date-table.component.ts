import {Component, OnInit} from '@angular/core';
import {DateRow} from '../date-row';
import {FireBaseProviderService} from '../../fire-base-provider.service';
import {IcalProviderService} from '../ical-provider.service';

@Component({
  selector: 'app-date-table',
  templateUrl: './date-table.component.html',
  styleUrls: ['./date-table.component.scss']
})
export class DateTableComponent implements OnInit {

  participators: string[] = ['Arcturus', 'Darli', 'Konstantin Khan', 'Mitras', 'Cain', 'Phyics Engine'];
  activeParticipator: string;
  days: DateRow[];
  dayThreshold = 10;

  constructor(public firebase: FireBaseProviderService, public ical: IcalProviderService) {
    const date: Date = new Date(Date.now());
    date.setDate(date.getDate() + 365);
    const tempDays = DateRange.getDates(new Date(Date.now()), date);
    this.days = [];
    for (const day of tempDays) {
      this.days.push(new DateRow(day));
    }
  }

  private static distanceToToday(date: Date): number {
    return (date.getTime() - new Date(Date.now()).getTime()) / (1000 * 3600 * 24);
  }

  public isDetermined(date: Date): boolean {
    for (const determinedDate of this.ical.dates) {
      if (determinedDate.toDateString() === date.toDateString()) {
        return true;
      }
    }
    return false;
  }

  public setActiveParticipator(participator: string) {
    if (this.activeParticipator === participator) {
      this.activeParticipator = undefined;
    } else {
      this.activeParticipator = participator;
    }
  }

  public getNextFullGreen(later: boolean): Date {
    for (const day of this.days) {
      if (day.red.length <= 0 && day.yellow.length <= 0 && (!later || DateTableComponent.distanceToToday(day.date) > this.dayThreshold)) {
        return day.date;
      }
    }
  }

  public getNextNoRed(later: boolean): Date {
    for (const day of this.days) {
      if (day.red.length <= 0 && (!later || DateTableComponent.distanceToToday(day.date) > this.dayThreshold)) {
        return day.date;
      }
    }
  }

  public getNextOneRed(later: boolean): Date {
    for (const day of this.days) {
      if (day.red.length <= 1 && (!later || DateTableComponent.distanceToToday(day.date) > this.dayThreshold)) {
        return day.date;
      }
    }
  }

  ngOnInit() {
    this.firebase.getDatesObservable().subscribe(dates => {
      this.updateDateRows(dates);
    });
  }

  public switchEntry(participator: string, dateRow: DateRow) {
    if (this.activeParticipator === participator) {
      dateRow.switch(participator);
      this.firebase.updateDate(dateRow);
    }
  }

  updateDateRows(dates: any[]) {
    for (const day of this.days) {
      for (const date of dates) {
        if (day.date.toDateString() === date.date) {
          day.yellow = date.yellow ? date.yellow : [];
          day.red = date.red ? date.red : [];
        }
      }
    }
  }

  public dayText(day: number) {
    switch (day) {
      case 0:
        return 'SO';
      case 1:
        return 'MO';
      case 2:
        return 'DI';
      case 3:
        return 'MI';
      case 4:
        return 'DO';
      case 5:
        return 'FR';
      case 6:
        return 'SA';
    }
  }

  public endlessScroll() {
    const lastDate = this.days[this.days.length - 1].date;
    const newDate = new Date(lastDate);
    newDate.setDate(newDate.getDate() + 365);
    const newDates = DateRange.getDates(lastDate, newDate);
    for (const day of newDates) {
      this.days.push(new DateRow(day));
    }
  }

  public isLastDayOfMonth(date: DateRow) {
    return new Date(date.date.getFullYear(), date.date.getMonth() + 1, 0).toDateString() === date.date.toDateString();
  }
}


class DateRange {

  static getDates(startDate: Date, endDate: Date): Date[] {
    const dates = [];
    let currentDate: Date = startDate;
    while (currentDate <= endDate) {
      dates.push(currentDate);
      currentDate = this.addDays(currentDate);
    }

    return dates;
  }

  private static addDays(currentDate) {
    const date = new Date(currentDate);
    date.setDate(date.getDate() + 1);
    return date;
  }
}
