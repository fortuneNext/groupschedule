export class DateRow {

  public date: Date;
  public yellow: string[];
  public red: string[];

  constructor(date: Date) {
    this.date = date;
    this.yellow = [];
    this.red = [];
  }

  private static deleteFromArray(array: string[], item: string) {
    const index = array.indexOf(item, 0);
    if (index > -1) {
      array.splice(index, 1);
    }
  }

  public makeRed(participator: string) {
    DateRow.deleteFromArray(this.yellow, participator);
    if (!this.red.includes(participator)) {
      this.red.push(participator);
    }
  }

  public makeYellow(participator: string) {
    DateRow.deleteFromArray(this.red, participator);
    if (!this.yellow.includes(participator)) {
      this.yellow.push(participator);
    }
  }

  public makeGreen(participator: string) {
    DateRow.deleteFromArray(this.red, participator);
    DateRow.deleteFromArray(this.yellow, participator);
  }

  public switch(participator: string) {
    if (this.yellow.includes(participator)) {
      this.makeGreen(participator);
    } else if (this.red.includes(participator)) {
      this.makeYellow(participator);
    } else {
      this.makeRed(participator);
    }
  }

  public isFullGreen(): boolean {
    return (this.yellow.length <= 0 && this.red.length <= 0);
  }

  public isNoRed(): boolean {
    return (this.red.length <= 0);
  }

  public isMaxOneRed(): boolean {
    return (this.red.length <= 1);
  }
}
