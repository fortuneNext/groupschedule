import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {DateTableComponent} from './date-table/date-table.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {AngularFireModule} from '@angular/fire';
import {environment} from '../environments/environment';
import {FireBaseProviderService} from '../fire-base-provider.service';
import {IcalProviderService} from './ical-provider.service';
import {HttpClientModule} from '@angular/common/http';
import {AngularFireFunctionsModule} from '@angular/fire/functions';

@NgModule({
  declarations: [
    AppComponent,
    DateTableComponent
  ],
  imports: [
    BrowserModule,
    InfiniteScrollModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    HttpClientModule,
    AngularFireFunctionsModule
  ],
  providers: [FireBaseProviderService, IcalProviderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
