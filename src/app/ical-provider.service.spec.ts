import {TestBed} from '@angular/core/testing';

import {IcalProviderService} from './ical-provider.service';

describe('IcalProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: IcalProviderService = TestBed.get(IcalProviderService);
    expect(service).toBeTruthy();
  });
});
