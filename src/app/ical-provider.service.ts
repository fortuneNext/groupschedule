import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class IcalProviderService {
  public dates: Date[] = [];

  constructor(http: HttpClient) {
    http.get(
      // tslint:disable-next-line:max-line-length
      'https://cors-anywhere.herokuapp.com/https://calendar.google.com/calendar/ical/3fvm3tbtkf7vi6j37qfaqij1rs%40group.calendar.google.com/private-b5bcdc0ac3490d926650949f402574a7/basic.ics',
      {responseType: 'text'}
    ).subscribe(res => {
      let searchIndex = 0;

      while (searchIndex < res.length && searchIndex >= 0 && res.indexOf('DTSTART', searchIndex) >= 0) {
        // debugger;
        const newElem = res.substr(res.indexOf('DTSTART', searchIndex) + 8,
          res.indexOf('DTEND', searchIndex + 1) - res.indexOf('DTSTART', searchIndex) - 9);
        const newDate = new Date();
        if (newElem.startsWith('VALUE=DATE:')) {
          newDate.setFullYear(Number(newElem.substr(11, 4)), Number(newElem.substr(15, 2)) - 1, Number(newElem.substr(17, 2)));
        } else {
          newDate.setFullYear(Number(newElem.substr(0, 4)), Number(newElem.substr(4, 2)) - 1, Number(newElem.substr(6, 2)));
        }
        this.dates.push(newDate);
        searchIndex = res.indexOf('DTEND', searchIndex + 1);
      }
    });
  }
}
