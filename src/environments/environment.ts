// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyBn--ln9_y5ogqws0rn8vHrtaP2-LjT3IA',
    authDomain: 'groupschedule-a3a2b.firebaseapp.com',
    databaseURL: 'https://groupschedule-a3a2b.firebaseio.com',
    projectId: 'groupschedule-a3a2b',
    storageBucket: 'groupschedule-a3a2b.appspot.com',
    messagingSenderId: '11120747116',
    appId: '1:11120747116:web:46ad7cee4e644ec54e6e49',
    measurementId: 'G-ZYEFTT8WD8'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
