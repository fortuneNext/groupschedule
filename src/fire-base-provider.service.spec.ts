import {TestBed} from '@angular/core/testing';

import {FireBaseProviderService} from './fire-base-provider.service';

describe('FireBaseProviderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FireBaseProviderService = TestBed.get(FireBaseProviderService);
    expect(service).toBeTruthy();
  });
});
