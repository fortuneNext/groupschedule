import * as functions from 'firebase-functions';

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
export const logIt = functions.https.onRequest((request, response) => {
  const original = request.query.text;
  console.log(original);
  // response.redirect(200, original);
  response.sendStatus(200);
});


export const warnIt = functions.https.onRequest((request, response) => {
  const original = request.query.text;
  console.warn(original);
  // response.redirect(200, original);
  response.sendStatus(200);
});
